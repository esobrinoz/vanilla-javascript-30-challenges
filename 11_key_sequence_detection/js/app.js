const controlVideo = document.querySelector('.contentVideo');
const video = controlVideo.querySelector('video');
const progressBar = controlVideo.querySelector('.progressBar');
const progressBarFilled = controlVideo.querySelector('.progressBar__filled');
const ranges = controlVideo.querySelectorAll("input[type='range']");
const togglePlayPauseBtn = controlVideo.querySelector('.togglePlayPauseBtn');
const play = controlVideo.querySelector('.playBtn');
const skipBtns = controlVideo.querySelectorAll("[data-skip]");

function toggleVideo(e) {
    e.stopPropagation();
    // Another way to do it. Just for fun
    const method = video.paused ? 'play' : 'pause';
    video[method]();
    if(!video.paused) video.muted = false;
}
function updateToggleBtn() {
    const text = this.paused ? 'play' : '||';
    togglePlayPauseBtn.textContent = text;
}
function skip() {
    video.currentTime += parseFloat(this.dataset.skip);
}
function handleRangeChanges() {
    if (this.name==='volume' && video.muted) video.muted = false;
    video[this.name] = this.value;
}
function handleBarProgress() {
    const percentage = (video.currentTime / video.duration) *100;
    progressBarFilled.style.flexBasis = `${percentage}%`;
}
function setVideoProgress(e) {
    const percentage = Math.round((e.offsetX / this.offsetWidth) *100);
    progressBarFilled.style.flexBasis = `${percentage}%`;
    video.currentTime = (video.duration* percentage) / 100;
}

video.addEventListener('click',toggleVideo);
video.addEventListener('play', updateToggleBtn);
video.addEventListener('pause', updateToggleBtn);
video.addEventListener('timeupdate', handleBarProgress);
togglePlayPauseBtn.addEventListener('click',toggleVideo);

skipBtns.forEach( button => button.addEventListener('click', skip));
ranges.forEach( inputRange => {
    inputRange.addEventListener('change', handleRangeChanges);
    inputRange.addEventListener('mousemove', handleRangeChanges);
});
progressBar.addEventListener('click', setVideoProgress);
