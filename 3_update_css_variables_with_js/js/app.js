const iputControls = document.querySelectorAll('input.control');
if (iputControls) iputControls.forEach( input => {
    input.addEventListener('change',onInputChanges);
    input.addEventListener('mousemove', onInputChanges);
});

function onInputChanges(e) {
    if (this.dataset.type == 'px') {
        document.documentElement.style.setProperty(`--${this.name}`,`${this.value}${this.dataset.type}`);
    } else if(this.dataset.type== 'rgb') {
        document.documentElement.style.setProperty(`--${this.name}`,`${this.dataset.type}(${this.value},210,56)`);
    }
}
