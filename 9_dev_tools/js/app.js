const body = document.querySelector('body');
const cities = [
    {
        city: 'London',
        temperature: '15°C',
    },
    {
        city: 'Madrid',
        temperature: '30°C'
    },
    {
        city: 'Amsterdam',
        temperature: '10°C',
    }
]
let word = 'video';
let doILearn = true;
function changeToRed() {
    body.style.backgroundColour= '#d4418e';
    body.style.backgroundImage= 'linear-gradient(315deg, #d4418e 0%, #0652c5 74%)';
}

// Regular
console.log('Hello');

// Interpolated
console.log('My name is %s', 'Eimard');
console.log(`I hope you find this ${word}`);

// Styled
console.log(`%c Useful`, 'font-size: 20px; color: blue');

// Warning
console.warn('I warn you ...');

// Error
console.error('I am good making mistakes ...');

// Info
console.info('but I inform you I learn from all of them');

// Testing
console.assert(doILearn, "Machine: 'I can confirm it!'");

// Clearing
// console.clear();

// DOM Elements
console.log(body);
console.dir(body);

// Grouping
console.group('English');
console.log('This is a group');
console.log('and it could be');
console.log('very useful');
console.groupEnd('English');
console.group('Spanish');
console.log('Esto es un grupo');
console.log('y puede ser');
console.log('muy util');
console.groupEnd('Spanish');

// Counting
console.count("Don't be annoying");
console.count("Don't be annoying");
console.count("Don't be annoying");
console.count("Don't be annoying");

// Timing
console.time('Fetching some data');
setTimeout( ()=> console.log('Data fetched'), 1000)
console.timeEnd('Fetching some data');

// Table
console.table(cities)

