let cities = [];
fetch("https://wft-geo-db.p.rapidapi.com/v1/geo/countries", {
	"method": "GET",
	"headers": {
		"x-rapidapi-key": "1a548aa4c0msh277b2b5da900231p10c9adjsnd795d8ad60ec",
		"x-rapidapi-host": "wft-geo-db.p.rapidapi.com"
	}
})
.then(response => response.json())
.then(response => {
  console.log(response.data);
  cities = response.data;
  rendeCities();
})
.catch(err => {
	console.error(err);
});

let listHtml= document.querySelector('.list');
const inputSearch = document.querySelector('.input-search');
inputSearch.addEventListener('change', findMatches);
inputSearch.addEventListener('keyup', findMatches);

function findMatches() {
  const regExp = new RegExp(this.value, 'gi');
  const mathArray = matchArray(cities, regExp);
  const htmlList = mathArray.map(city => {
    const cityAux = city.name.replace(regExp,`<span class="highlight">${this.value}</span>`)
    return `
      <li class="list__element"> ${cityAux}</li>
    `;
  }).join('');
  listHtml.innerHTML = htmlList;
}

function rendeCities() {
  const htmlList = cities.map(city => {
    return `
      <li class="list__element"> ${city.name}</li>
    `;
  }).join('');
  listHtml.innerHTML = htmlList;
}

function matchArray(cities, rexExp) {
  return cities.filter( city => city.name.match(rexExp));
}