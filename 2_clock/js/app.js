
const maxDegrees = 360;
const maxSeconds= 60;
let clock;
function setDate() {
    const dateNow = new Date();
    const seconds = dateNow.getSeconds();
    const secondsDegrees = ((seconds / 60)*360) + 90;
    const secondsHand = document.querySelector('span.seconds');
    if (secondsHand) secondsHand.style.transform =`rotate(${secondsDegrees}deg)`;
    const minutes = dateNow.getMinutes();
    const minutesDegrees = ((minutes / 60)*360) + 90;
    const minutesHand = document.querySelector('span.minutes');
    if (minutesHand) minutesHand.style.transform =`rotate(${minutesDegrees}deg)`;
    const hours = dateNow.getHours();
    const hoursDegrees = ((hours / 12)*360) + 90;
    const hoursHand = document.querySelector('span.hours');
    if (hoursHand) hoursHand.style.transform =`rotate(${hoursDegrees}deg)`; 
    const audio = document.querySelector('audio');
    if (audio) {
        audio.currentTime = 0.5;
        audio.play();
    }
    if(!clock) {
        clock = document.querySelector('.contentClock');
        clock.style.transform = "scale(1)";
    }
}
setInterval(setDate, 1000)