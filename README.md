<!-- https://github.com/[username]/[reponame]/blob/[branch]/image.jpg?raw=true) -->
![alt text](https://gitlab.com/esobrinoz/vanilla-javascript-30-challenges/blob/development/projectCover.png?raw=true)

# Vanilla Javascript 30 challenges

30 javascripts challenges built with VANILLA JAVASCRIPT


NO Frameworks + NO Compilers + NO Libraries

The purpose is to get to know deeply how Javascript works behind the scenes and without any library or framework.

# 1) Piano Challenge
### Run
Open the index.html file within '1_piano_kit' folder
To play the sound, press the keys: A, S, D, F, G, H, J, K, or L 
### See video
Play the video 'Vanilla_Javascript _Piano_Challenge_1' that you can find within the '1_piano_kit' folder.

# 2) Clock Challenge
### Run
Open the index.html file within '2_clock' folder
A clock will be displayed with real time 
### See video
Play the video 'Vanilla_Javascript_Clock_Challenge_2' that you can find within the '2_clock' folder.

# 3) Updating CSS variables with Javascript Challenge
### Run
Open the index.html file within '3_update_css_variables_with_js' folder
Move the ranges to change the colour and font size of the elements displayed.
### See video
Play the video 'Vanilla_Javascript_Updating-css-variables_Challenge_3' that you can find within the '3_update_css_variables_with_js' folder.


# 4) XMLrequest and array manipualtion Challenge
### Run
Open the index.html file within '4_xmlrequest_array_manipulation' folder
I fetched all the currencies from skyscanner api and map, filter, sort and calculate (reduce) the sum of all the values using vanilla javascipt.
### See video
Play the video 'Vanilla_Javascript_XMLRequest_ArrayManypulation_Challenge_4' that you can find within the '4_xmlrequest_array_manipulation' folder.

# 5) CSS Transition and Flex Challenge
### Run
Open the index.html file within '5_image_CSS_flex_transition' folder
Move the mouse over the different columns to be displayed with transitions and each player statiscts (goals, games and age).

### See video
Play the video 'Vanilla_Javascript_CSS_transition_flex_Challenge_5' that you can find within the '5_image_CSS_flex_transition' folder.

# 6) AJAX and Search bar
### Run
Open the index.html file within '6_Ajax_SearchBar' folder
I fetched a list of cities and redered it within an html list. Listening to the 'keyup' and 'change' events I filter, render and highlight the word searched and the new results within the list.

### See video
Play the video 'Vanilla_Javascript_Ajax_searchBar_Challenge_6' that you can find within the '6_Ajax_SearchBar' folder.

# 7) Array manipulation
### Run
Open the index.html file within '7_array_manipulation' folder
I fetched all the currencies from skyscanner api and made use of some(), every(), find(), findIndex(), etc

### See video
Play the video 'Vanilla_Javascript_Array_manipulation_Challenge_7' that you can find within the '7_array_manipulation' folder.

# 8) HTML Canvas
### Run
Open the index.html file within '8_html_canvas' folder
I implemented a free drawing canvas (and erase featur) where the line width and coloyr changes dynamically acording to the rainbow colours.
### See video
Play the video 'Vanilla_Javascript_Html_Canvas_Challenge_8' that you can find within the '8_html_canvas' folder.

# 9) Dev tools
### Run
Open the index.html file within '9_dev_tools' folder
I show several ways of debugging our code using the console: console.log, console.table, console.count, console.group, console.dir, break on attribute modification, console.info, console.warning, console.error, interpolation, etc).
### See video
Play the video 'Vanilla_Javascript_dev_tools_Challenge_9' that you can find within the '9_dev_tools' folder.

# 10) Multiple checkboxes
### Run
Open the index.html file within '10_multiple_checkboxes' folder
*Press SHIFT when selecting a checkbox to check all the checkboxes in between

### See video
Play the video 'Vanilla_Javascript_multiple_checkboxes_Challenge_10' that you can find within the '10_multiple_checkboxes' folder.

# 11) Key Sequencie Detection
### Run
Open the index.html file within '11_key_sequence_detection' folder
Features: Pause/Play, progress bar, skip (+10s / -25s), volume and speed control, css transitions.

### See video
Play the video 'Vanilla_Javascript_multiple_key_secuence_detection_Challenge_11' that you can find within the '11_key_sequence_detection' folder.