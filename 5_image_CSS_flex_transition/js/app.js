const wrapperTitle = document.querySelector('.contentTitle');
document.documentElement.addEventListener('mouseover', hideTitle);
document.documentElement.addEventListener('mouseout', showTitle);
const panels = document.querySelectorAll('.sub-panel');
panels.forEach( panel => panel.addEventListener('mouseover', openPanel));
panels.forEach( panel => panel.addEventListener('mouseout', closePanel));
panels.forEach( panel => panel.addEventListener('transitionend', handleTransition));
function showTitle() {
    wrapperTitle.classList.remove('hide');
}
function hideTitle() {
    wrapperTitle.classList.add('hide');
}
function openPanel() {
    this.classList.add('open');
}
function closePanel() {
    this.classList.remove('open');
}
function handleTransition(e) {
    if (e && e.propertyName.includes('flex')) {
        if (e.currentTarget.classList.contains('open')) this.classList.add('panel-active');
        if (!e.currentTarget.classList.contains('open')) {
            this.classList.remove('panel-active');
        }
    }
}