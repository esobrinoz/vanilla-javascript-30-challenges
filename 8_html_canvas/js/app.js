
const canvas = document.querySelector('#canvas');
const canvasCtx= canvas.getContext('2d');
canvas.width =window.innerWidth;
canvas.height =window.innerHeight;
canvasCtx.strokeStyle = '#8b0000';
canvasCtx.lineCap = 'round';
canvasCtx.lineJoin = 'round';
canvasCtx.lineWidth = 50;
canvasCtx.globalCompositeOperation  = 'source-over';
let drawing = false;
let lastX = 0;
let lastY = 0;
let hue = 0;
let direction = true;
let paintMode = true;
const btnPaint = document.querySelector('.paint');
const btnErase = document.querySelector('.erase');
btnPaint.addEventListener('click', paint);
btnErase.addEventListener('click', erase);

function paint() {
    btnErase.classList.remove('active');
    this.classList.add('active');
    canvasCtx.globalCompositeOperation  = 'source-over';
}
function erase() {
    btnPaint.classList.remove('active');
    this.classList.add('active');
    canvasCtx.globalCompositeOperation  = 'destination-out';
}


canvas.addEventListener('mousedown', (e)=> {
    [lastX, lastY] = [e.offsetX, e.offsetY];
    drawing=true;
});

canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mouseup', ()=> drawing=false);
canvas.addEventListener('mouseout', ()=> drawing=false);
function draw(e) {
    if(!drawing) return;
    console.log(e);
    canvasCtx.beginPath();
    canvasCtx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    canvasCtx.moveTo(lastX, lastY);
    canvasCtx.lineTo(e.offsetX,  e.offsetY);
    canvasCtx.stroke();
    [lastX, lastY] = [e.offsetX, e.offsetY];
    hue++;
    if (hue >=360) hue = 0;
    
    if (canvasCtx.lineWidth >=100 || canvasCtx.lineWidth <=1) direction =!direction;
    if (direction) canvasCtx.lineWidth++;
    else canvasCtx.lineWidth--;
}