

function removeActiveClass(e){
    if(e.propertyName == 'transform') {
        this.classList.remove('active');
    }
}
function playPiano(e) {
    const keySelected = e.keyCode;
    console.log('Emlio ', keySelected)
    const audio =  document.querySelector(`audio[data-key="${keySelected}"]`);
    const keyPressed = document.querySelector(`.key[data-key="${keySelected}"]`);
    if (keyPressed) {
        keyPressed.classList.add('active');
    }
    if(!audio) return;
    audio.currentTime= 0;
    audio.play();
}

const keys = document.querySelectorAll('.key');
keys.forEach(key => key.addEventListener('transitionend', removeActiveClass));
window.addEventListener('keydown', playPiano);
