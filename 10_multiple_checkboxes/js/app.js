const checkboxes = document.querySelectorAll(".form input[type='checkbox']");
console.dir(checkboxes);
checkboxes.forEach( checkbox => checkbox.addEventListener('click', handleClick));
let lastInputChecked;
function handleClick(e) {
    let inBetween = false;
    this.labels[0].classList.toggle('checked');
    if(e.shiftKey && this.checked) {
        checkboxes.forEach(item => {
            if (item === this || item ===lastInputChecked) {
                inBetween = !inBetween;
            }
            if (inBetween) {
                item.checked = true;
                item.labels[0].classList.add('checked');
            } 
        });
    }
    lastInputChecked = this;
}