const data = null;

const xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
	if (this.readyState === this.DONE) {
        const data = JSON.parse(this.responseText);
        renderData(data);
	}
});
xhr.open("GET", "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/reference/v1.0/currencies");
xhr.setRequestHeader("x-rapidapi-key", "1a548aa4c0msh277b2b5da900231p10c9adjsnd795d8ad60ec");
xhr.setRequestHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");

xhr.send(data);

function renderData(data) {
    if (!data || !data['Currencies'] || !data['Currencies'].length) return;
    const div = document.querySelector('#wrapper');
    const list = document.createElement('ul');
    div.appendChild(list)
    let dataList = data['Currencies'].map(item => {
        return {
            symbol: item['Symbol'],
            code: item['Code'],
            separator: item['ThousandsSeparator'],
            amount: 0
        }
    })
    dataList = createElements(dataList, list);
    const answer1 = document.querySelector('#result1');
    answer1.textContent = someUsesDollarSymbol(dataList) ? 'YES' : 'NO';;
    const answer2 = document.querySelector('#result2');
    const currency =getCurrencyWithDollarSymbol(dataList);
    answer2.textContent = `Symbol: ${currency.symbol} | Code: ${currency.code} | Separator: ${currency.separator}`
    const answer3 = document.querySelector('#result3');
    answer3.textContent = allUseCommaSeparator(dataList) ? 'YES' : 'NO';
}

function createElements(dataList, list) {
    dataList.forEach(item => {
        const element = document.createElement('li');
        const text = document.createElement('p');
        text.textContent = item['symbol'];
        text.classList.add('list__item__title');
        element.classList.add('list__item');
        const currencyLabel = document.createElement('p');
        currencyLabel.classList.add('list__item__txt');
        currencyLabel.textContent = 'Code:';
        const thousandsSeparatorLabel = document.createElement('p');
        thousandsSeparatorLabel.classList.add('list__item__txt');
        thousandsSeparatorLabel.textContent = 'Thousands Separator:';
        const valuecurrency = document.createElement('span');
        valuecurrency.textContent = ` ${item['code']}`;
        valuecurrency.classList.add('list__item__value');
        const valuethousandsSeparator = document.createElement('span');
        valuethousandsSeparator.textContent = ` ${item['separator']}`;
        valuethousandsSeparator.classList.add('list__item__value');
        const ammountLabel = document.createElement('p');
        ammountLabel.textContent = 'Amount: ';
        ammountLabel.classList.add('list__item__txt');
        const ammountValue = document.createElement('span');
        item.amount = Math.floor(Math.random() *1001);
        ammountValue.textContent =` ${item.amount} ${item['symbol']}`;
        ammountValue.classList.add('list__item__value');
        ammountLabel.appendChild(ammountValue);
        element.appendChild(text);
        element.appendChild(currencyLabel);
        element.appendChild(thousandsSeparatorLabel);
        element.appendChild(ammountLabel);
        currencyLabel.appendChild(valuecurrency);
        thousandsSeparatorLabel.appendChild(valuethousandsSeparator);
        list.appendChild(element);
    });
    return dataList;
}

function someUsesDollarSymbol(dataList) {
    return dataList.some(item=> item.symbol.includes('$'));
}

function getCurrencyWithDollarSymbol(dataList) {
    return dataList.find(item=> item.symbol !== '$' && item.symbol.includes('$'));
}


function allUseCommaSeparator(dataList) {
    return dataList.every(item=> item.separator === ',');
}
